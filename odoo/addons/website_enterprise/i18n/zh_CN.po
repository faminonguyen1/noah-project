# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * website_enterprise
# 
# Translators:
# waveyeung <waveyeung@qq.com>, 2017
# Jeffery CHEN <jeffery9@gmail.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.saas~18+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-09-20 11:33+0000\n"
"PO-Revision-Date: 2017-09-20 11:33+0000\n"
"Last-Translator: Jeffery CHEN <jeffery9@gmail.com>, 2017\n"
"Language-Team: Chinese (China) (https://www.transifex.com/odoo/teams/41243/zh_CN/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: zh_CN\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. module: website_enterprise
#: model:ir.ui.view,arch_db:website_enterprise.external_snippets
msgid "Form Builder"
msgstr "表单生成器"

#. module: website_enterprise
#: model:ir.ui.view,arch_db:website_enterprise.user_navbar
msgid "Website"
msgstr "网站"
