EDI for Mexico with CFDI version 3.3
====================================

Allow the user to generate the EDI document for Mexican invoicing in version 3.3
and manage the complement to payments version 1.0.

Based in the `SAT documentation <https://goo.gl/TWU3NZ>`_ to CFDI version 3.3
and `Payment Complement <https://goo.gl/9HqhCa>`_.

Here was loaded the SAT catalogs to allow generate both CFDI documents. And
the fields required to complete the document.

For more information visit the `Odoo Documentation <https://goo.gl/azwQDU>`_
