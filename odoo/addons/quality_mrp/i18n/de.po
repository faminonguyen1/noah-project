# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * quality_mrp
# 
# Translators:
# Gab_Odoo <gab@odoo.com>, 2017
# darenkster <inactive+darenkster@transifex.com>, 2017
# Martin Trigaux <mat@odoo.com>, 2017
# Stefan Hartenfels <stefan.hartenfels@camptocamp.com>, 2017
# Wolfgang Taferner <wolfgang.transifex@service.wt-io-it.at>, 2017
# Tina Rittmüller <tr@ife.de>, 2017
# JackTheHunter <Maximilian.N98@gmail.com>, 2017
# Henry Mineehen <info@mineehen.de>, 2017
# Constantin Ehrenstein <transifex@skiller.eu>, 2017
# Ermin Trevisan <trevi@twanda.com>, 2017
# Renzo Meister <info@jamotion.ch>, 2017
# Ralf Hilgenstock <rh@dialoge.info>, 2017
# Bülent Tiknas <btiknas@googlemail.com>, 2017
# DE T1 <e2f8846@yahoo.com>, 2017
# Katharina Moritz <kmo@e2f.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 11.0+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-10-24 09:23+0000\n"
"PO-Revision-Date: 2017-10-24 09:23+0000\n"
"Last-Translator: Katharina Moritz <kmo@e2f.com>, 2017\n"
"Language-Team: German (https://www.transifex.com/odoo/teams/41243/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_production_view_form_inherit_quality
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_inherit_quality
msgid ""
"<span class=\"o_stat_text text-success\" attrs=\"{'invisible': [('quality_check_fail', '=', True)]}\">Quality Checks</span>\n"
"                    <span class=\"o_stat_text text-danger\" attrs=\"{'invisible': [('quality_check_fail', '!=', True)]}\">Quality Checks</span>"
msgstr ""
"<span class=\"o_stat_text text-success\" attrs=\"{'invisible': [('quality_check_fail', '=', True)]}\">Qualitätsprüfungen</span>\n"
"                    <span class=\"o_stat_text text-danger\" attrs=\"{'invisible': [('quality_check_fail', '!=', True)]}\">Qualitätsprüfungen</span>"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_production_view_form_inherit_quality
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_inherit_quality
msgid "<span class=\"o_stat_text\">Quality Alert</span>"
msgstr "<span class=\"o_stat_text\">Qualitätsalarm</span>"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_production_quality_alert_ids
msgid "Alerts"
msgstr "Alarm"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_allow_producing_quantity_change
msgid "Allow Changes to the Produced Quantity"
msgstr ""

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_quality_point_test_type_allow_registration
msgid "Allow Registration"
msgstr "Anmeldung zulassen"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet_menu
msgid "Block"
msgstr "Sperren"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_check_ids
msgid "Check"
msgstr "Scheck"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_production_check_ids
msgid "Checks"
msgstr "Kontrollen"

#. module: quality_mrp
#. openerp-web
#: code:addons/quality_mrp/static/src/xml/widget_template.xml:9
#, python-format
msgid "Clear"
msgstr "Suche löschen"

#. module: quality_mrp
#: model:ir.actions.act_window,help:quality_mrp.mrp_workorder_action_tablet
msgid "Click to start a new work order."
msgstr "Anklicken, um neuen Arbeitsauftrag zu starten"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_component_id
#: model:ir.model.fields,field_description:quality_mrp.field_quality_check_component_id
#: model:ir.model.fields,field_description:quality_mrp.field_quality_point_component_id
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Component"
msgstr "Komponente"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Continue"
msgstr "Weiter"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_control_date
msgid "Control Date"
msgstr "Prüfdatum"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.quality_alert_view_form_inherit_mrp
msgid "Create Work Order Message"
msgstr "Mitteilung zu Arbeitsauftrag erstellen"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_current_quality_check_id
msgid "Current Quality Check"
msgstr "Aktuelle Qualitätsprüfung"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_inherit_quality
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Date"
msgstr "Datum"

#. module: quality_mrp
#: model:ir.model.fields,help:quality_mrp.field_mrp_workorder_component_uom_id
#: model:ir.model.fields,help:quality_mrp.field_quality_check_component_uom_id
msgid "Default Unit of Measure used for all stock operation."
msgstr "Standard Mengeneinheit (ME) für alle Bestandsbuchungen."

#. module: quality_mrp
#: selection:quality.point,worksheet:0
msgid "Do not update page"
msgstr "Seite nicht aktualisieren"

#. module: quality_mrp
#: code:addons/quality_mrp/models/quality.py:105
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_qty_done
#: model:ir.model.fields,field_description:quality_mrp.field_quality_check_qty_done
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_inherit_quality
#, python-format
msgid "Done"
msgstr "Erledigt"

#. module: quality_mrp
#. openerp-web
#: code:addons/quality_mrp/static/src/xml/widget_template.xml:8
#, python-format
msgid "Edit"
msgstr "Bearbeiten"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Fail"
msgstr "Fehlgeschlagen"

#. module: quality_mrp
#: code:addons/quality_mrp/models/quality.py:112
#, python-format
msgid "Failure"
msgstr "Fehler"

#. module: quality_mrp
#: code:addons/quality_mrp/models/mrp_workorder.py:163
#, python-format
msgid "Failure Message"
msgstr "Fehlermitteilung"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Finish steps"
msgstr "Schritte abschließen"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_finished_product_check_ids
msgid "Finished Product Check"
msgstr "Abgeschlossene Produktprüfung"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_quality_check_finished_product_sequence
msgid "Finished Product Sequence Number"
msgstr "Abgeschlossene, fortlaufende Produktnummer"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_inherit_quality
msgid "Finished Steps"
msgstr "Abgeschlossene Schritte"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_is_first_step
msgid "Is First Step"
msgstr "Ist erster Schritt"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_is_last_step
msgid "Is Last Step"
msgstr "Ist letzter Schritt"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_is_last_lot
msgid "Is Last lot"
msgstr "Ist letztes Fertigungslos"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_lot_id
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Lot"
msgstr "Fertigungslos"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Lot:"
msgstr "Fertigungslos:"

#. module: quality_mrp
#: model:ir.model,name:quality_mrp.model_mrp_production
msgid "Manufacturing Order"
msgstr "Fertigungsauftrag"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Mark as Done"
msgstr "Als erledigt markieren"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_measure
msgid "Measure"
msgstr "Werte"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_measure_success
msgid "Measure Success"
msgstr "Erfolg messen"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Measure:"
msgstr "Werte:"

#. module: quality_mrp
#: code:addons/quality_mrp/models/mrp_workorder.py:270
#, python-format
msgid "Menu"
msgstr "Menü"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_move_line_id
#: model:ir.model.fields,field_description:quality_mrp.field_quality_check_move_line_id
msgid "Move Line"
msgstr "Buchungszeile"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Next"
msgstr "Weiter"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_note
msgid "Note"
msgstr "Notiz"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.quality_check_failure_message
msgid "OK"
msgstr "OK"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_quality_alert_workorder_id
#: model:ir.model.fields,field_description:quality_mrp.field_quality_check_workorder_id
msgid "Operation"
msgstr "Vorgang"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_inherit_quality
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Operator"
msgstr "Ausführender"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_quality_check_parent_id
msgid "Parent Quality Check"
msgstr "Übergeordnete Qualitätsprüfung"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_quality_point_routing_id
msgid "Parent Routing"
msgstr "Übergeordneter Arbeitsplan"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Pass"
msgstr "Bestanden"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Pause"
msgstr "Pause"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_picture
#: model:ir.model.fields,field_description:quality_mrp.field_quality_check_picture
msgid "Picture"
msgstr "Bild"

#. module: quality_mrp
#: code:addons/quality_mrp/models/quality.py:120
#, python-format
msgid "Picture Uploaded"
msgstr "Bild hochgeladen"

#. module: quality_mrp
#: model:ir.actions.act_window,help:quality_mrp.mrp_workorder_action_tablet
msgid "Plan some work orders from your manufacturing orders."
msgstr "Planen Sie die Arbeitsaufträge mithilfe Ihrer Fertigungsaufträge."

#. module: quality_mrp
#: code:addons/quality_mrp/models/mrp_workorder.py:84
#: code:addons/quality_mrp/models/mrp_workorder.py:171
#, python-format
msgid ""
"Please ensure the quantity to produce is nonnegative and does not exceed the"
" remaining quantity."
msgstr ""
"Bitte prüfen Sie, dass die zu produzierende Menge keinen negativen Wert "
"enthält und nicht die verbleibende Menge übersteigt."

#. module: quality_mrp
#: code:addons/quality_mrp/models/mrp_workorder.py:90
#, python-format
msgid "Please enter a Lot/SN."
msgstr "Bitte geben Sie ein Fertigungslos bzw. eine SN ein."

#. module: quality_mrp
#: code:addons/quality_mrp/models/mrp_workorder.py:92
#, python-format
msgid "Please enter a positive quantity."
msgstr "Bitte geben Sie einen positiven Wert für die Menge ein."

#. module: quality_mrp
#: code:addons/quality_mrp/models/mrp_workorder.py:86
#, python-format
msgid "Please upload a picture."
msgstr "Bitte laden Sie ein Bild hoch."

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Previous"
msgstr "Vorherige"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_inherit_quality
msgid "Process"
msgstr "Prozess"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_quality_alert_production_id
#: model:ir.model.fields,field_description:quality_mrp.field_quality_check_production_id
msgid "Production Order"
msgstr "Fertigungsauftrag"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Production Workcenter"
msgstr "Arbeitsplatz"

#. module: quality_mrp
#: model:ir.model,name:quality_mrp.model_quality_alert
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_quality_alert_ids
#: model:ir.ui.view,arch_db:quality_mrp.mrp_production_view_form_inherit_quality
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workcenter_view_kanban_inherit_quality_mrp
msgid "Quality Alert"
msgstr "Qualitätsalarm"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_production_quality_alert_count
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_quality_alert_count
msgid "Quality Alert Count"
msgstr "Qualitätsalarmanzahl"

#. module: quality_mrp
#: model:ir.model,name:quality_mrp.model_quality_check
msgid "Quality Check"
msgstr "Qualitätskontrolle"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_production_quality_check_fail
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_quality_check_fail
msgid "Quality Check Fail"
msgstr "Fehlgeschlagene Qualitätsprüfung"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_production_quality_check_todo
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_quality_check_todo
msgid "Quality Check Todo"
msgstr "Vorzunehmende Qualitätsprüfung"

#. module: quality_mrp
#: model:ir.actions.act_window,name:quality_mrp.quality_check_action_mo
#: model:ir.actions.act_window,name:quality_mrp.quality_check_action_wo
#: model:ir.ui.view,arch_db:quality_mrp.mrp_production_view_form_inherit_quality
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_inherit_quality
msgid "Quality Checks"
msgstr "Qualitätskontrollen"

#. module: quality_mrp
#: model:ir.model,name:quality_mrp.model_quality_point
msgid "Quality Point"
msgstr "Qualitätspunkt"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet_menu
msgid "Quality alert"
msgstr "Qualitätsalarm"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Quantity"
msgstr "Menge"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Record production"
msgstr "Datensatzerstellung"

#. module: quality_mrp
#: code:addons/quality_mrp/models/quality.py:99
#, python-format
msgid "Register component(s)"
msgstr "Komponente(n) registrieren"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_component_remaining_qty
msgid "Remaining Quantity for Component"
msgstr "Verbleibende Menge für Komponente"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_user_id
msgid "Responsible"
msgstr "Verantwortlich"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_quality_check_result
msgid "Result"
msgstr "Ergebnis"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "SN"
msgstr "SN"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "SN:"
msgstr "SN:"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet_menu
msgid "Scrap"
msgstr "Ausschuss melden"

#. module: quality_mrp
#: selection:quality.point,worksheet:0
msgid "Scroll to specific page"
msgstr "Zu bestimmter Seite scrollen"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Skip"
msgstr "Überspringen"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_skip_completed_checks
msgid "Skip Completed Checks"
msgstr "Abgeschlossene Prüfungen überspringen"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_skipped_check_ids
msgid "Skipped Check"
msgstr "Prüfung übersprungen"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_quality_state
#: model:ir.model.fields,field_description:quality_mrp.field_quality_check_quality_state_for_summary
msgid "Status"
msgstr "Status"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_quality_point_operation_id
msgid "Step"
msgstr "Schrittweise Erhöhung"

#. module: quality_mrp
#: model:ir.actions.act_window,name:quality_mrp.action_quality_mrp_show_steps
#: model:ir.ui.view,arch_db:quality_mrp.mrp_routing_view_form_inherit_quality
msgid "Steps"
msgstr "Schritte"

#. module: quality_mrp
#: code:addons/quality_mrp/models/quality.py:112
#, python-format
msgid "Success"
msgstr "Erfolgreich"

#. module: quality_mrp
#. openerp-web
#: code:addons/quality_mrp/static/src/xml/widget_template.xml:18
#, python-format
msgid "Take a Picture"
msgstr "Bild aufnehmen"

#. module: quality_mrp
#: model:ir.model,name:quality_mrp.model_quality_point_test_type
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_test_type
msgid "Test Type"
msgstr "Testtyp"

#. module: quality_mrp
#: model:ir.model.fields,help:quality_mrp.field_quality_point_routing_id
msgid ""
"The routing contains all the Work Centers used and for how long. This will "
"create work orders afterwardswhich alters the execution of the manufacturing"
" order. "
msgstr ""
"Der Arbeitsplan enthält alle verwendeten Arbeitsplätze und deren "
"Nutzungsdauer. Dadurch werden Arbeitsaufträge erstellt, mit welchen die "
"Ausführung des Fertigungsauftrags geändert wird."

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_quality_check_title
msgid "Title"
msgstr "Titel"

#. module: quality_mrp
#: code:addons/quality_mrp/models/quality.py:105
#, python-format
msgid "To Do"
msgstr "Zu erledigen"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_component_tracking
msgid "Tracking"
msgstr "Nachverfolgung"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_quality_point_code
msgid "Type of Operation"
msgstr "Vorgangstyp"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet_menu
msgid "Unblock"
msgstr "Entsperren"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_component_uom_id
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_norm_unit
#: model:ir.model.fields,field_description:quality_mrp.field_quality_check_component_uom_id
msgid "Unit of Measure"
msgstr "Mengeneinheit"

#. module: quality_mrp
#. openerp-web
#: code:addons/quality_mrp/static/src/xml/widget_template.xml:11
#, python-format
msgid "Uploading..."
msgstr "Hochladen …"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet
msgid "Validate"
msgstr "Bestätigen"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_quality_alert_workcenter_id
#: model:ir.model.fields,field_description:quality_mrp.field_quality_check_workcenter_id
msgid "Work Center"
msgstr "Arbeitsplatz"

#. module: quality_mrp
#: model:ir.model,name:quality_mrp.model_mrp_workorder
msgid "Work Order"
msgstr "Arbeitsauftrag"

#. module: quality_mrp
#: code:addons/quality_mrp/models/quality.py:53
#, python-format
msgid "Work Order Messages"
msgstr "Mitteilungen zu Arbeitsaufträgen"

#. module: quality_mrp
#: model:ir.actions.act_window,name:quality_mrp.mrp_workorder_action_tablet
msgid "Work Orders"
msgstr "Arbeitsaufträge"

#. module: quality_mrp
#: model:ir.ui.view,arch_db:quality_mrp.mrp_workorder_view_form_tablet_menu
msgid "Workorder Actions"
msgstr "Aktionen der Arbeitsaufträge"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_quality_point_worksheet
msgid "Worksheet"
msgstr "Arbeitsblatt"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_quality_point_worksheet_page
msgid "Worksheet Page"
msgstr "Arbeitsblattseite"

#. module: quality_mrp
#: model:ir.model.fields,field_description:quality_mrp.field_mrp_workorder_worksheet_page
msgid "Worksheet page"
msgstr "Arbeitsblattseite"

#. module: quality_mrp
#: code:addons/quality_mrp/models/mrp_workorder.py:387
#, python-format
msgid "You should provide a lot for the final product"
msgstr "Sie sollten für das fertige Produkt ein Fertigungslos angeben."

#. module: quality_mrp
#: code:addons/quality_mrp/models/mrp_production.py:93
#: code:addons/quality_mrp/models/mrp_workorder.py:385
#, python-format
msgid "You still need to do the quality checks!"
msgstr "Sie müssen noch die Qualitätskontrolle durchführen!"
