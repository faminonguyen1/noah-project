# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * web_mobile
# 
# Translators:
# Dorin Hongu <dhongu@gmail.com>, 2017
# Cozmin Candea <office@terrabit.ro>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.saas~18+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-09-20 11:33+0000\n"
"PO-Revision-Date: 2017-09-20 11:33+0000\n"
"Last-Translator: Cozmin Candea <office@terrabit.ro>, 2017\n"
"Language-Team: Romanian (https://www.transifex.com/odoo/teams/41243/ro/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: ro\n"
"Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));\n"

#. module: web_mobile
#. openerp-web
#: code:addons/web_mobile/static/src/xml/contact_sync.xml:9
#, python-format
msgid "Add to"
msgstr "Adaugă la"

#. module: web_mobile
#. openerp-web
#: code:addons/web_mobile/static/src/xml/user_menu.xml:7
#, python-format
msgid "Add to Home Screen"
msgstr ""

#. module: web_mobile
#. openerp-web
#: code:addons/web_mobile/static/src/xml/contact_sync.xml:10
#, python-format
msgid "Mobile"
msgstr "Mobil"

#. module: web_mobile
#. openerp-web
#: code:addons/web_mobile/static/src/js/user_menu.js:56
#, python-format
msgid "No shortcut for App Switcher"
msgstr ""

#. module: web_mobile
#. openerp-web
#: code:addons/web_mobile/static/src/xml/user_menu.xml:10
#, python-format
msgid "Switch/Add Account"
msgstr "Schimbă / adaugă cont"
