# -*- coding: utf-8 -*-

from . import account
from . import account_invoice
from . import account_payment
from . import certificate
from . import product
from . import res_country
from . import res_partner
